package ru.tsc.avramenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.IUserRepository;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return list.stream()
                .filter(u -> u.getLogin().equals(login))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        return list.stream()
                .filter(u -> u.getEmail().equals(email))
                .limit(1)
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User removeUserById(@NotNull final String id) {
        @Nullable final User user = findById(id);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User removeUserByLogin(@NotNull final String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

}