package ru.tsc.avramenko.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.command.AbstractCommand;
import java.util.Collection;

public class ArgumentsDisplayCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @Nullable
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list arguments.";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> arguments = serviceLocator.getCommandService().getArguments();
        for (final AbstractCommand argument : arguments)
            System.out.println(argument.arg());
    }

}