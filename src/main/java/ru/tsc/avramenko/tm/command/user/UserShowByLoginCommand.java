package ru.tsc.avramenko.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.command.AbstractUserCommand;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public class UserShowByLoginCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return "user-show-by-login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show user by login.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN: ");
        @Nullable final String login = TerminalUtil.nextLine();
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        showUser(user);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}