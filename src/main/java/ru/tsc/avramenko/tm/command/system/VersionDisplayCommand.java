package ru.tsc.avramenko.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.command.AbstractCommand;

public class VersionDisplayCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @Nullable
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program version.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.1.7");
    }

}